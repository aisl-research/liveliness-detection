import numpy as np
import argparse
import cv2
from yolo_utils import infer_image, show_image

FLAGS = []

def spoofing(img_):
    parser = argparse.ArgumentParser()

    parser.add_argument('-m', '--model-path',
                        type=str,
                        default='./yolov3-coco/')

    parser.add_argument('-w', '--weights',
                        type=str,
                        default='./yolov3-coco/yolov3.weights')

    parser.add_argument('-cfg', '--config',
                        type=str,
                        default='./yolov3-coco/yolov3.cfg')

    parser.add_argument('-l', '--labels',
                        type=str,
                        default='./yolov3-coco/coco-labels')

    parser.add_argument('-c', '--confidence',
                        type=float,
                        default=0.5,
                        )

    parser.add_argument('-th', '--threshold',
                        type=float,
                        default=0.3,
                        help='The threshold to use when applying the Non-Max Suppresion')

    parser.add_argument('-t', '--show-time',
                        type=bool,
                        default=True,
                        help='Show the time taken to infer each image.')



    FLAGS, unparsed = parser.parse_known_args()


    # Get the labels
    labels = open(FLAGS.labels).read().strip().split('\n')

    # Intializing colors to represent each label uniquely
    colors = np.random.randint(0, 255, size=(len(labels), 3), dtype='uint8')

    # Load the weights and configutation to form the pretrained YOLOv3 model
    net = cv2.dnn.readNetFromDarknet(FLAGS.config, FLAGS.weights)

    # Get the output layer names of the model
    layer_names = net.getLayerNames()
    layer_names = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    height, width = img_.shape[:2]
    img_, _, _, classids, _ = infer_image(net, layer_names, height, width, img_, colors, labels, FLAGS)

    if not classids:
        return False

    elif labels[classids[-1]] =='cell phone' or labels[classids[-1]] =='tvmonitor' or labels[classids[0]] =='cell phone' or labels[classids[0]] =='tvmonitor':
        print('No-spoofing')
        return True

    else:
        print(labels[classids[-1]])
        return False